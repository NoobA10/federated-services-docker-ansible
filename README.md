# Instructions

## 1. Make sure SSH connection to your server is working

Use publickey authentication

## 2. Update hosts

fill *hosts* file with your server configuration

## 3. Install Ansible on your control node

## 4. Edit the variables for each role

Edit the variables e.g. `roles/pixelfed/vars/main.yml`
 set passwords and other configs

## 5. Run playbook

- `ansible-playbook roles_playbook.yml -K`

If you have encrypted secrets, run

- `ansible-playbook roles_playbook.yml -K --ask-vault-pass`

## Setup your reverve proxy and DNS settings


